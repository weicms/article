<?php

define('ROOT', realpath(dirname(__DIR__ . '/../../..')));

if (!defined('WP_ONLY')) {
    laravel_run();
}

function laravel_run()
{

    require ROOT . '/bin/laravel/bootstrap/autoload.php';

    $app = require_once ROOT . '/bin/laravel/bootstrap/app.php';

    /*
    |--------------------------------------------------------------------------
    | Run The Application
    |--------------------------------------------------------------------------
    |
    | Once we have the application, we can simply call the run method,
    | which will execute the request and send the response back to
    | the client's browser allowing them to enjoy the creative
    | and wonderful application we have prepared for them.
    |
    */

    $kernel = $app->make('Illuminate\Contracts\Http\Kernel');

    $response = $kernel->handle(
        $request = Illuminate\Http\Request::capture()
    );

    $response->send();

    $kernel->terminate($request, $response);

}
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'joomla');

define('RELOCATE', true);

/** MySQL database username */
define('DB_USER', 'joomla');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'Nz`mm-e/B,6wph11:G^tG[;`2rodTun#,HUFO4<Xe6__0Iv7F_8lMFGM,H+)O+ma');
define('SECURE_AUTH_KEY', 'M/:`{`<Re-Ll^MdN~iyrptT!Gw6S1:y&541cIW:lUTyM<j=z2:7z>=fDN~?:=_A[');
define('LOGGED_IN_KEY', '|UU}.-f2x<JYoU_[j#FyMAvLO5$mhB`gUt=?YinZV2Gi<sQR?wfi]*eOFWDwnXgx');
define('NONCE_KEY', '@,K=)am]1PYftW9NMYj]Y^}MY+{s`aEd<C#?xX*sK~Qd#69Y}[edT^YL{Pijf`(M');
define('AUTH_SALT', 'eM/:Elcf/Kb!%chOGYxy6S{DJK78i=h;C`nd>>.uvhF{ZqaOplZTXq+n!{MJZL54');
define('SECURE_AUTH_SALT', 'xA E^UYkih,SQfyYgJiX GcLRB5b@!Y],4^H$(6Eeau)Be+iY)pB<T-<YYN*tQW,');
define('LOGGED_IN_SALT', 'aaa s5N4YER;$ZhF(gq  HyFY}Umt=r|!d|2ejayP>VYUS[y`Zw7 9U6}RXEZw2B');
define('NONCE_SALT', 'k{opv#DW)U!SKk1@i?#?WE^-q,4|oOuEk]9<:`L1/lbHyw&|(jg|v>}[ pqLBk74');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/');
}

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
